
public class Table {
	static char table[][] = new char [3][3];
	static char name;
	public   boolean run(char name) {
		Table.name = name;
		if(checkWin()) {
			return true;
		}
		return false;
	}
	
	private static boolean checkWin() {
		if (checkRow()) {
			return true;
		}
		if (checkCol()) {
			return true;
		}
		if (checkX()) {
			return true;
		}
		return false;
	}

	private static boolean checkRow(int rowInd) {
		for (int colInd = 0; colInd < table[rowInd].length; colInd++) {
			if (table[rowInd][colInd] != Table.name)
				return false;
		}
		return true;
	}

	private static boolean checkRow() {
		for (int rowInd = 0; rowInd < table.length; rowInd++) {
			if (checkRow(rowInd))
				return true;
		}
		return false;
	}
	private static boolean checkCol(int colInd) {
		for (int rowInd = 0; rowInd < table[colInd].length; rowInd++) {
			if (table[rowInd][colInd] != Table.name)
				return false;
		}
		return true;
	}

	private static boolean checkCol() {
		for (int colInd = 0; colInd < table[0].length; colInd++) {
			if (checkCol(colInd))
				return true;
		}
		return false;
	}
	private static boolean checkX() {
		if (checkX1()) {
			return true;
		}
		if (checkX2()) {
			return true;
		}
		return false;
	}

	private static boolean checkX1() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][i] != Table.name)
				return false;
		}
		return true;
	}

	private static boolean checkX2() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][2 - i] != Table.name)
				return false;
		}
		return true;
	}
}
