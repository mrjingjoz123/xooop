import java.util.Scanner;

public class Game {
	static Player player = new Player();
	static Table table = new Table();
	static int row, col;

	public void run() {

		player.name = 'X';
		startGame();
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			input();
			if(table.run(player.name)) {
				showTable();
				break;
			}
			switchPlayer();
		}
		showWin();
		while (showNext()) {
			runNext();
		}
	
	}

	private void runNext() {
		startGame();
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			input();
			if(table.run(player.name)) {
				showTable();
				break;
			}
			switchPlayer();
		}
		showWin();
		while (showNext()) {
			runNext();
		}
		
	}

	public void startGame() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Table.table[i][j] = '-';
			}
		}
		player.turn = 0;
	}

	public void showWelcome() {
		System.out.println("Welcome to XO Game");
	}

	private void showTable() {
		System.out.println("  1 2 3");
		for (int i = 0; i < Table.table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < Table.table.length; j++) {
				System.out.print(" " + Table.table[i][j]);
			}
			System.out.println();
		}
	}

	private void showTurn() {
		System.out.println(player.name + " turn ");

	}

	private void input() {

		Scanner kb = new Scanner(System.in);
		System.out.print("Input row , col: ");
		row = kb.nextInt();
		col = kb.nextInt();
		if ((row > 3 || row < 1) || (col > 3 || col < 1)) {
			System.out.println("Sorry!!  wrong input  please input row , col again ");
			input();
		} else if (Table.table[row - 1][col - 1] != '-') {
			System.out.println("Sorry!!  same input  please input row , col again ");
			input();
		}
		Table.table[row - 1][col - 1] = player.name;
	}


	private static void switchPlayer() {
		if (player.name == 'X') {
			player.name = 'O';
		} else if (player.name == 'O') {
			player.name = 'X';
		}
		player.turn++;
	}
	private static void showWin() {
		if (isDraw()) {
			System.out.println("Draw");
		} else {
			System.out.println(player.name + " Win...");
		}
	}
	private static boolean isDraw() {
		if (player.turn == 8)
			return true;
		return false;
	}
	private static boolean showNext() {
		if (player.name == 'X') {
			player.name = 'O';
		} else if (player.name == 'O') {
			player.name = 'X';
		}
		Scanner kb = new Scanner(System.in);
		System.out.print("Do yo want to play XO game again : Y or N ");
		char input = kb.next().charAt(0);
		if(input == 'Y') {
			return true;
		}
		showBye();
		return false;
	}

	private static void showBye() {
		System.out.println("Bye Bye...");

	}

}
